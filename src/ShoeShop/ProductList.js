import React from "react";
import ItemShoe from "./ItemShoe";

export default function ProductList({
  productList,
  handleAddToCart,
  handleDetail,
}) {
  return (
    <div className="row m-0">
      {productList.map((item, index) => {
        return (
          <ItemShoe
            key={index}
            item={item}
            handleAddToCart={handleAddToCart}
            handleDetail={handleDetail}
          />
        );
      })}
    </div>
  );
}
