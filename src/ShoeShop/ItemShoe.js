import React from "react";

export default function ItemShoe({ item, handleAddToCart, handleDetail }) {
  let { image, name, price } = item;
  return (
    <div className="col-4 my-3">
      <div className="card" style={{ height: "100%" }}>
        <img src={image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <h6>{price}$</h6>
          <button
            onClick={() => handleAddToCart(item)}
            className="btn btn-primary my-2 mx-1"
          >
            Add to cart
          </button>
          <button
            onClick={() => handleDetail(item)}
            data-toggle="modal"
            data-target="#exampleModal"
            className="btn btn-success"
          >
            Detail
          </button>
        </div>
      </div>
    </div>
  );
}
