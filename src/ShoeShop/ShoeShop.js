import React, { useState } from "react";
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import Modal from "./Modal";
import ProductList from "./ProductList";

export default function ShoeShop() {
  let [productList, setProductList] = useState(dataShoe);
  let [cart, setCart] = useState([]);
  let [productDetail, setProductDetail] = useState([]);

  let handleAddToCart = (shoe) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return item.id === shoe.id;
    });
    if (index === -1) {
      let newShoe = { ...shoe, quantity: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].quantity++;
    }
    setCart(cloneCart);
  };
  let handleDelete = (id) => {
    if (window.confirm("Are you want to delete your product?") === true) {
      let cloneCart = [...cart];
      let index = cloneCart.findIndex((item) => {
        return item.id === id;
      });
      cloneCart.splice(index, 1);
      setCart(cloneCart);
    }
  };
  let handleChangeQuantity = (id, quantity) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return item.id === id;
    });
    if (quantity === 1) {
      cloneCart[index].quantity += quantity;
    } else {
      cloneCart[index].quantity > 1
        ? (cloneCart[index].quantity += quantity)
        : cloneCart.splice(index, 1);
    }
    setCart(cloneCart);
  };
  let handleDetail = (shoe) => {
    setProductDetail(shoe);
  };
  return (
    <div>
      <h2>Shoes Shop</h2>
      <div className="row m-0">
        <div className="col-7">
          <ProductList
            productList={productList}
            handleAddToCart={handleAddToCart}
            handleDetail={handleDetail}
          />
        </div>
        <div className="col-5 cart">
          <Cart
            handleDelete={handleDelete}
            cart={cart}
            handleChangeQuantity={handleChangeQuantity}
          />
        </div>
      </div>
      <Modal productDetail={productDetail} />
    </div>
  );
}
